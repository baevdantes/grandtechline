$('.left-sidebar__navigation-list-item-drop').click((event) => {
    event.preventDefault();
});

$('.left-sidebar__navigation-list-item_js').click(function () {
    // $('.left-sidebar__navigation-drop-list').slideUp(100).removeClass('left-sidebar__navigation-drop-list_active');
    if ($(this).find('.left-sidebar__navigation-drop-list').hasClass('left-sidebar__navigation-drop-list-item_active')) {
        $(this).find('.left-sidebar__navigation-drop-list').slideUp(100).removeClass('left-sidebar__navigation-drop-list-item_active');
    } else {
        $(this).find('.left-sidebar__navigation-drop-list').slideDown(100).addClass('left-sidebar__navigation-drop-list-item_active');
    }
});


/* Left Sidebar Carousel */

$('.left-sidebar__carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: true,
    items: 1,
    dotClass: 'left-sidebar__carousel-dot',
    dotsClass: 'left-sidebar__carousel-dots',
});

$('.main__carousel').owlCarousel({
    stagePadding: 100,
    loop: true,
    margin: 5,
    nav: true,
    dots: false,
    items: 1,
    navContainerClass: 'main__carousel-nav',
    navClass: ['main__carousel-nav-prev', 'main__carousel-nav-next'],
    navText: ['', ''],
    stageOuterClass: 'main__carousel-stage'
});

$('.right-sidebar__works-carousel').owlCarousel({
    stagePadding: 35,
    loop: true,
    margin: 0,
    nav: true,
    dots: false,
    items: 1,
    navContainerClass: 'right-sidebar__works-carousel',
    navClass: ['right-sidebar__works-carousel-prev', 'right-sidebar__works-carousel-next'],
    navText: ['', ''],
    stageOuterClass: 'right-sidebar__works-carousel-stage'
});

if (window.innerWidth > 800) {
    (function () {
        var a = document.querySelector('.left-sidebar'), b = null, K = null, Z = 0, P = 0, N = 0;  // если у P ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента, если у N — нижний край дойдёт до нижнего края элемента. Может быть отрицательным числом
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);

        function Ascroll() {
            var Ra = a.getBoundingClientRect(),
                R1bottom = document.querySelector('.main').getBoundingClientRect().bottom;
            if (Ra.bottom < R1bottom) {
                if (b == null) {
                    var Sa = getComputedStyle(a, ''), s = '';
                    for (var i = 0; i < Sa.length; i++) {
                        if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                            s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                        }
                    }
                    b = document.createElement('div');
                    b.className = "stop";
                    b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                    a.insertBefore(b, a.firstChild);
                    var l = a.childNodes.length;
                    for (var i = 1; i < l; i++) {
                        b.appendChild(a.childNodes[1]);
                    }
                    a.style.height = b.getBoundingClientRect().height + 'px';
                    a.style.padding = '0';
                    a.style.border = '0';
                }
                var Rb = b.getBoundingClientRect(),
                    Rh = Ra.top + Rb.height,
                    W = document.documentElement.clientHeight,
                    R1 = Math.round(Rh - R1bottom),
                    R2 = Math.round(Rh - W);
                if (Rb.height > W) {
                    if (Ra.top < K) {  // скролл вниз
                        if (R2 + N > R1) {  // не дойти до низа
                            if (Rb.bottom - W + N <= 0) {  // подцепиться
                                b.className = 'sticky';
                                b.style.top = W - Rb.height - N + 'px';
                                Z = N + Ra.top + Rb.height - W;
                            } else {
                                b.className = 'stop';
                                b.style.top = -Z + 'px';
                            }
                        } else {
                            b.className = 'stop';
                            b.style.top = -R1 + 'px';
                            Z = R1;
                        }
                    } else {  // скролл вверх
                        if (Ra.top - P < 0) {  // не дойти до верха
                            if (Rb.top - P >= 0) {  // подцепиться
                                b.className = 'sticky';
                                b.style.top = P + 'px';
                                Z = Ra.top - P;
                            } else {
                                b.className = 'stop';
                                b.style.top = -Z + 'px';
                            }
                        } else {
                            b.className = '';
                            b.style.top = '';
                            Z = 0;
                        }
                    }
                    K = Ra.top;
                } else {
                    if ((Ra.top - P) <= 0) {
                        if ((Ra.top - P) <= R1) {
                            b.className = 'stop';
                            b.style.top = -R1 + 'px';
                        } else {
                            b.className = 'sticky';
                            b.style.top = P + 'px';
                        }
                    } else {
                        b.className = '';
                        b.style.top = '';
                    }
                }
                window.addEventListener('resize', function () {
                    a.children[0].style.width = getComputedStyle(a, '').width
                }, false);
            }
        }
    })();

    (function () {
        var a = document.querySelector('.right-sidebar'), b = null, K = null, Z = 0, P = 0, N = 0;  // если у P ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента, если у N — нижний край дойдёт до нижнего края элемента. Может быть отрицательным числом
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);

        function Ascroll() {
            var Ra = a.getBoundingClientRect(),
                R1bottom = document.querySelector('.main').getBoundingClientRect().bottom;
            if (Ra.bottom < R1bottom) {
                if (b == null) {
                    var Sa = getComputedStyle(a, ''), s = '';
                    for (var i = 0; i < Sa.length; i++) {
                        if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                            s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                        }
                    }
                    b = document.createElement('div');
                    b.className = "stop";
                    b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                    a.insertBefore(b, a.firstChild);
                    var l = a.childNodes.length;
                    for (var i = 1; i < l; i++) {
                        b.appendChild(a.childNodes[1]);
                    }
                    a.style.height = b.getBoundingClientRect().height + 'px';
                    a.style.padding = '0';
                    a.style.border = '0';
                }
                var Rb = b.getBoundingClientRect(),
                    Rh = Ra.top + Rb.height,
                    W = document.documentElement.clientHeight,
                    R1 = Math.round(Rh - R1bottom),
                    R2 = Math.round(Rh - W);
                if (Rb.height > W) {
                    if (Ra.top < K) {  // скролл вниз
                        if (R2 + N > R1) {  // не дойти до низа
                            if (Rb.bottom - W + N <= 0) {  // подцепиться
                                b.className = 'sticky';
                                b.style.top = W - Rb.height - N + 'px';
                                Z = N + Ra.top + Rb.height - W;
                            } else {
                                b.className = 'stop';
                                b.style.top = -Z + 'px';
                            }
                        } else {
                            b.className = 'stop';
                            b.style.top = -R1 + 'px';
                            Z = R1;
                        }
                    } else {  // скролл вверх
                        if (Ra.top - P < 0) {  // не дойти до верха
                            if (Rb.top - P >= 0) {  // подцепиться
                                b.className = 'sticky';
                                b.style.top = P + 'px';
                                Z = Ra.top - P;
                            } else {
                                b.className = 'stop';
                                b.style.top = -Z + 'px';
                            }
                        } else {
                            b.className = '';
                            b.style.top = '';
                            Z = 0;
                        }
                    }
                    K = Ra.top;
                } else {
                    if ((Ra.top - P) <= 0) {
                        if ((Ra.top - P) <= R1) {
                            b.className = 'stop';
                            b.style.top = -R1 + 'px';
                        } else {
                            b.className = 'sticky';
                            b.style.top = P + 'px';
                        }
                    } else {
                        b.className = '';
                        b.style.top = '';
                    }
                }
                window.addEventListener('resize', function () {
                    a.children[0].style.width = getComputedStyle(a, '').width
                }, false);
            }
        }
    })();
}

$('.burger_js').click(function () {
    $('.left-sidebar_wrapper').addClass('left-sidebar_wrapper_show');
    $('.overlay').addClass('overlay_active');
});

$('.overlay, .left-sidebar__close').click(function () {
    $('.left-sidebar_wrapper').removeClass('left-sidebar_wrapper_show');
    $('.overlay').removeClass('overlay_active');
});

$('.left-sidebar__contacts-item-city').click(function () {
   $('.left-sidebar__contacts-change-modal').toggleClass('left-sidebar__contacts-change-modal_showed');
});

jQuery(function($){
    $(document).mouseup(function (e){ // событие клика по веб-документу
        const div = $(".left-sidebar__contacts-change-modal"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0 || e.target === $('.left-sidebar__contacts-item-city'))


        { // и не по его дочерним элементам
            div.removeClass('left-sidebar__contacts-change-modal_showed'); // скрываем его
        }
    });
});