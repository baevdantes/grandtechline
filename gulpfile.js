const gulp = require('gulp');
const watch = require('gulp-watch');
const sass = require('gulp-sass');
const autoPrefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const minify = require('gulp-minify');
const browserSync = require('browser-sync');

const reload = browserSync.reload;

const paths = {
    html: ['dist/*.html'],
    css: ['dist/scss/*.scss'],
    js: ['dist/**/*.js']
};

gulp.task('styles', function () {
    gulp.src(['!dist/scss/variables.scss','dist/scss/*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoPrefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/css'))
});

gulp.task('scripts', function () {
    return gulp.src('dist/js/scripts.js')
        .pipe(concat('index.js'))
        .pipe(minify())
        .pipe(gulp.dest('dist/'))
});

gulp.task('libs-js', function () {
    return gulp.src('dist/libs/**/*.js')
        .pipe(concat('libs.js'))
        .pipe(minify())
        .pipe(gulp.dest('./dist/'))
});

gulp.task('libs-css', function () {
    return gulp.src('dist/libs/**/*.css')
        .pipe(concat('libs.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./dist/css'))
});

gulp.task('watch', function () {
    gulp.watch('dist/scss/*.scss', ['styles']);
    gulp.watch('dist/js/*.js', ['scripts']);
});

gulp.task('default', ['bundle-css', 'bundle-js', 'watch']);

gulp.task('bundle-js', ['libs-js', 'scripts']);

gulp.task('bundle-css', ['libs-css', 'styles']);
